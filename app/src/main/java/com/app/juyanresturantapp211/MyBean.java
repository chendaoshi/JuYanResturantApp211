package com.app.juyanresturantapp211;

/**
 * Created by Administrator on 2019/10/14.
 */

public class MyBean {

    /**
     * name : 12345678912
     * password : 123456
     */

    private String name;
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
