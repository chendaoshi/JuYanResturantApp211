package com.app.juyanresturantapp211;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.ReferenceQueue;

public class LoginActivity extends AppCompatActivity {

    EditText mTvPassword,mTvUsername;
    Button btnClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initView();
        initEvent();


    }

    private void initEvent() {
        //点击事件
        btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regist();

            }
        });

    }

    private void regist() {
        //请求队列
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        //创建请求
        JSONObject object = new JSONObject();
        try {
            object.put("name", mTvUsername.getText().toString());
            object.put("password", mTvPassword.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST, "HTTP://106.12.30.77:8083/freshmenapp/user/regist", object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        boolean success = jsonObject.optBoolean("success", false);
                        if (success == true) {
                            Toast.makeText(LoginActivity.this, "注册成功", Toast.LENGTH_SHORT).show();


                        } else {
                            Toast.makeText(LoginActivity.this, "注册失败", Toast.LENGTH_SHORT).show();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(LoginActivity.this, "网络访问错误", Toast.LENGTH_SHORT).show();

                    }


                });


        requestQueue.add(request);

    }

    private void initView() {
        mTvUsername= (EditText) findViewById(R.id.et_username);
        mTvPassword= (EditText) findViewById(R.id.et_password);
        btnClick= (Button) findViewById(R.id.bt_click);
    }
}
